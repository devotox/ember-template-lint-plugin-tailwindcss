# ember-template-lint-plugin-tailwindcss 🌬

![npm version](https://badgen.net/npm/v/ember-template-lint-plugin-tailwindcss)
![license](https://badgen.net/npm/license/ember-template-lint-plugin-tailwindcss)
![downloads](https://badgen.net/npm/dw/ember-template-lint-plugin-tailwindcss)
![Dependabot](https://badgen.net/badge/icon/dependabot?icon=dependabot&label)
![Volta Managed](https://img.shields.io/static/v1?label=volta&message=managed&color=yellow&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QAeQC6AMEpK7AhAAAACXBIWXMAAAsSAAALEgHS3X78AAAAB3RJTUUH5AMGFS07qAYEaAAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAFmSURBVDjLY2CgB/g/j0H5/2wGW2xyTAQ1r2DQYOBgm8nwh+EY6TYvZtD7f9rn5e81fAGka17GYPL/esObP+dyj5Cs+edqZsv/V8o//H+z7P+XHarW+NSyoAv8WsFszyKTtoVBM5Tn7/Xys+zf7v76vYrJlPEvAwPjH0YGxp//3jGl/L8LU8+IrPnPUkY3ZomoDQwOpZwMv14zMHy8yMDwh4mB4Q8jA8OTgwz/L299wMDyx4Mp9f9NDAP+bWVwY3jGsJpB3JaDQVCEgYHlLwPDfwYWRqVQJgZmHoZ/+3PPfWP+68Mb/Pw5sqUoLni9ipuRnekrAwMjA8Ofb6K8/PKBF5nU7RX+Hize8Y2DOZTP7+kXogPy1zrH+f/vT/j/Z5nUvGcr5VhJioUf88UC/59L+/97gUgDyVH4YzqXxL8dOs/+zuFLJivd/53HseLPPHZPsjT/nsHi93cqozHZue7rLDYhUvUAADjCgneouzo/AAAAAElFTkSuQmCC&link=https://volta.sh)
![ts](https://flat.badgen.net/badge/-/TypeScript?icon=typescript&label&labelColor=blue&color=555555)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

A [Tailwind CSS 🌬](https://tailwindcss.com/) plugin for [ember-template-lint 👨🏻](https://github.com/ember-template-lint/ember-template-lint).

![Example of class-order rule](docs/rule/class-order.gif)

## Install

```sh
yarn add -D ember-template-lint-plugin-tailwindcss
```

```js
// .template-lintrc.js
module.exports = {
  plugins: ['ember-template-lint-plugin-tailwindcss'],
};
```

## Recommended configuration

A recommended configuration is available. To use it, merge the following object to your `.template-lintrc.js` file:

```js
// .template-lintrc.js
module.exports = {
  plugins: ['ember-template-lint-plugin-tailwindcss'],
  extends: [
    'recommended', // this comes from ember-template-lint
    'ember-template-lint-plugin-tailwindcss:recommended'
  ]
};
```

The `ember-template-lint-plugin-tailwindcss:recommended` set will apply [these rules](/lib/config/recommended.js).

## Configuration

You can use all the standard [ember-template-lint configuration options](https://github.com/ember-template-lint/ember-template-lint/blob/master/docs/configuration.md). For example project wide:

```js
// .template-lintrc.js
module.exports = {
  plugins: ['ember-template-lint-plugin-tailwindcss'],
  extends: ['recommended', 'ember-template-lint-plugin-tailwindcss:recommended'],
  rules: {
    'class-wrap': [
      'error',
      {
        classesPerLine: 5 // this overrides default configuration
      }
    ], 
  },
};
```

### Configuration options

#### class-wrap
* boolean - `true` to enable / `false` (default) to disable
* object -- An object with the following keys:
  * `classesPerLine` -- integer|null: Put a line-wrap in the class attribute after N classes

Note: This rule tends to fight with [ember-template-lint-plugin-prettier](https://github.com/ember-template-lint/ember-template-lint-plugin-prettier), so if you are using prettier plugin, it's recommended to have `class-wrap` disabled.

#### class-order
* boolean - `true` to enable / `false` to disable
* object -- object: of marchers, sorters and groups:
  * `matchers` -- object:
    * `key`: name of the matcher will be used in `groups.[].matchBy`
    * `value`: function that returns true if given class belongs to given group; false otherwise
  * `sorters` -- object:
    * `key`: name of the sorter will be used in `groups.[].sortBy`
    * `value`: function that takes two strings `a` and `b` as input and returns number greater than zero if `a > b` and less than zero if `a < b`
  * `groups` -- array of objects:
    * `matchBy`: name of respective matcher
    * `sortBy`: name of respective sorter
    * `order`: determines sorting of all the groups; lower number means group will be applied earlier
  * `linebreakBetweenGroups` -- `false | object`
    * `false`: do not add linebreak between each group
    * `object`: add a forced linebreak between each group
      * `indent`: number of how many spaces from class attribute start we should indent
      * `onlyWithHint` -- `false | string`:
        * `false`: this option is _always_ enabled
        * `string`: enable this option _only_ when `class` attribute _starts_ with this string
  * `disableForMustaches` -- `false | true`:
    * `true`: won't attempt to insert any linebreaks if `class` attribute contains any mustache statement
    * `false`: even `class` attributes containing mustache statements will be line broken if other settings says so
  * `warnForConcat` -- `false | true`:
    * `true`: will mark concatenated dynamic class attributes as problematic
    * `false`: won't mark concatenated dynamic class attributes as problematic

`class-order` rule will look into the `groups` array and pick each group one by one. It will go through all the classes and use `matchBy` matcher on them keeping only those that return truthy value (rest will be carried to the next group). Then the `sortBy` function will be used to rearrange the order of those classes. In the end the groups of sorted classes are concatenated according to the `order` parameter ascending.

This means that last entry in `groups` should always have `matchBy: "all"`, otherwise this plugin will throw away classes that did not match any groups.

If you omit `matchBy: "all"` from your groups you can also use this plugin to make sure that only classes matched by other matchers in your groups can be used. Classes that don't match any groups will be discarded.

##### Defaults

```
{
  warnForConcat: false // // Note: This is the default to preserve backwards compatibility for `2.x` version; Will be flipped to `true` in `5.x`
  disableForMustaches: true, // Note: This is the default to preserve backwards compatibility for `2.x` version; Will be flipped to `false` in `5.x`
  linebreakBetweenGroups: {
    onlyWithHint: `\n`,
    indent: 2,
  }
}
```

This will allow following:

```hbs
<div class="gap-4 grid grid-cols-2 lg:grid-cols-5 md:grid-cols-3">
  foo
</div>

<div
  class="                           <-- has to start with `\n` for that options to take place
    gap-4 grid grid-cols-2          <-- group-1
    lg:grid-cols-5 md:grid-cols-3   <-- group-2
  "
>
  bar
</div>
```

###### matchers

- `tailwindClass`: Matches if given class comes from tailwind (excluding variants)
- `tailwindVariant`: Matches if given class is tailwind variant
- `all`: Matches anything

###### sorters

- `alphabet`: Sorts by alphabet descending
- `classList`: Sorts by predefined list of classes that is trying to mimic groupping by logical blocks

###### groups

```js
groups: [
  {
    matchBy: "tailwindClass",
    sortBy: "alphabet",
    order: 2,
  },
  {
    matchBy: "tailwindVariant",
    sortBy: "alphabet",
    order: 3,
  },
  {
    matchBy: "all",
    sortBy: "alphabet",
    order: 1,
  },
],
```

## Credits

 - [Headwind vscode extension](https://marketplace.visualstudio.com/items?itemName=heybourn.headwind) for the initial list for sorting the classes & the idea.
 - [ember-template-lint-plugin-prettier](https://github.com/ember-template-lint/ember-template-lint-plugin-prettier) for the guidance on how-to build an _ember-template-lint-plugin_.
