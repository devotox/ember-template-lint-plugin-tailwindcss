import { nodeResolve } from "@rollup/plugin-node-resolve";

import { babel } from "@rollup/plugin-babel";

// eslint-disable-next-line node/no-missing-import
import { Addon } from "@embroider/addon-dev/rollup";

const addon = new Addon({
  srcDir: "src",
  destDir: "lib",
});

const extensions = [".js", ".ts"];

const transpilation = [
  nodeResolve({ resolveOnly: ["./"], extensions }),
  babel({ babelHelpers: "bundled", extensions }),
  addon.dependencies(),
];

export default [
  {
    input: "src/index.ts",
    output: { ...addon.output(), entryFileNames: "[name].js" },
    plugins: [...transpilation],
  },
];
