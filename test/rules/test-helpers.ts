import { describe, it, beforeEach } from "vitest";
import { generateRuleTests as _generateTests } from "ember-template-lint";

import plugin from "../../lib/index.js";

export function generateRuleTests(testInfo: any) {
  return _generateTests({
    groupMethodBefore: beforeEach,
    groupingMethod: describe,
    testMethod: it,
    plugins: [plugin],

    ...testInfo,
  });
}
