import { generateRuleTests } from "../test-helpers";

generateRuleTests({
  name: "class-order",

  config: {
    matchers: {
      "starts-with-a-dash": (item: string) => item.match(/^-/),
    },
    groups: [
      {
        matchBy: "starts-with-a-dash",
        sortBy: "alphabet",
        order: 1,
      },
      {
        matchBy: "all",
        sortBy: "alphabet",
        order: 2,
      },
    ],
  },

  good: [`<div class="-m-2 -p-1 bg-white rounded-lg"></div>`],

  bad: [
    {
      template: `<div class="-m-2 bg-white -p-1 rounded-lg"></div>`,
      fixedTemplate: `<div class="-m-2 -p-1 bg-white rounded-lg"></div>`,
      result: {
        message:
          "HTML class attribute sorting is: '-m-2 bg-white -p-1 rounded-lg', but should be: '-m-2 -p-1 bg-white rounded-lg'",
        line: 1,
        column: 5,
        isFixable: true,
        source: `class="-m-2 bg-white -p-1 rounded-lg"`,
      },
    },
  ],
});
