import { generateRuleTests } from "../test-helpers";

generateRuleTests({
  name: "class-order",

  config: {
    groups: [
      {
        sortBy: "alphabet",
        matchBy: "all",
      },
    ],
  },

  good: [`<div class="bg-white md:flex p-1 rounded-lg"></div>`],

  bad: [
    {
      template: `<div class="md:flex bg-white rounded-lg p-2 lg:my-2"></div>`,
      fixedTemplate: `<div class="bg-white lg:my-2 md:flex p-2 rounded-lg"></div>`,
      result: {
        message:
          "HTML class attribute sorting is: 'md:flex bg-white rounded-lg p-2 lg:my-2', but should be: 'bg-white lg:my-2 md:flex p-2 rounded-lg'",
        line: 1,
        column: 5,
        isFixable: true,
        source: `class="md:flex bg-white rounded-lg p-2 lg:my-2"`,
      },
    },
  ],
});
