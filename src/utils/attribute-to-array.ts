export default function attributeToArray(
  classAttribute: string
): Array<string> {
  return classAttribute.replace(/\s+/g, " ").trim().split(" ");
}
