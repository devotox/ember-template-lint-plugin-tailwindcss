export function getGap(size: number): string {
  return `\n` + " ".repeat(size);
}
